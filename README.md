 # Seeed HTTP Proxy

## Building

Note: This project uses `dep` to manage dependency. If you don't have it, please install by following
official instruction.

```
$ dep ensure
$ make
```