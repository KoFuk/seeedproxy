package main

import (
	"strings"
)

func split(hostname string) []string {
	result := make([]string, strings.Count(hostname, ".")+1)
	cindex := 0
	for i := len(hostname) - 1; i >= 0; i-- {
		if hostname[i] == '.' {
			result[cindex] = hostname[i+1:]
			cindex++
		}
	}
	result[cindex] = hostname
	return result
}
