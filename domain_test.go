package main

import (
	"testing"
)

func equalsAll(actual, expected []string) bool {
	if len(actual) != len(expected) {
		return false
	}
	for n, s := range actual {
		if s != actual[n] {
			return false
		}
	}
	return true
}

func TestSplit(t *testing.T) {
	s := split("1.2.3.4")
	if !equalsAll(s, []string{"4", "3.4", "2.3.4", "1.2.3.4"}) {
		t.Fatal("Fail")
	}
}
